/*----------------------------------------------------------------------------*/
#ifndef __MY1FALLOUT_ITEM_H__
#define __MY1FALLOUT_ITEM_H__
/*
https://falloutmods.fandom.com/wiki/SAVE.DAT_File_Format#Inventory_item_format
*/
/*----------------------------------------------------------------------------*/
#define ITEM_INVENTORY 0xFFFFFFFF
/* inuse_location bit flags */
#define ITEM_USE_HAND1 0x01000000
#define ITEM_USE_HAND2 0x02000000
#define ITEM_USE_ARMOR 0x04000000
/*----------------------------------------------------------------------------*/
typedef struct _item_t {
	unsigned int quantity;
	unsigned int unknown0; /* changes when one is in use? */
	unsigned int location; /* -1 for inventory */
	unsigned int unknowns_0[2]; /* always 0? */
	unsigned int specials_0[2];
	unsigned int unknowns_1[2];
	unsigned int image_onground;
	/* offset:0x28 (@40) */
	/* bitflag msb 0x01 (r) 0x02 (l) 0x04 (body) : unknown0 changes */
	/* (why lsb 0x08?) => assume for weapon? */
	unsigned int inuse_location;
	unsigned int map_level; /* zero for inventory? irrelevant */
	/* offset:0x30 (@48) */
	unsigned int objectid;
	unsigned int checkFF0; /* always -1? */
	unsigned int unknowns_2[2];
	unsigned int check000;
	unsigned int scriptid; /* -1 if none */
	unsigned int checkFF1; /* always -1? */
	/* offset:0x4c (@76) */
	unsigned int bag_count; /* 0 for non-containers */
	unsigned int bag_items[2]; /* 0 for non-containers */
	unsigned int check001; /* always 0? */
	/* offset:0x5c  (@92=23x unsigned int) */
	/* Not present for armor, containers, or drugs? */
	unsigned int ammo_count; /* for ammo, current */
	unsigned int ammo_type; /* -1 for no ammo, for weapons only! */
	/* size:0x64 (@100) */
} item_t;
/*----------------------------------------------------------------------------*/
#define ITEM_BASE_COUNT 23
#define ITEM_MORE_COUNT 2
/*----------------------------------------------------------------------------*/
#define ITEM_TYPE_UNKNOWN -1
#define ITEM_TYPE_ARMOR 0
#define ITEM_TYPE_CONTAINER 1
#define ITEM_TYPE_DRUG 2
#define ITEM_TYPE_CONSUMABLE 3
#define ITEM_TYPE_WEAPON 8
#define ITEM_TYPE_AMMO 9
#define ITEM_TYPE_MISC 10
/*----------------------------------------------------------------------------*/
#define ITEM_NAMESZ 32
/*----------------------------------------------------------------------------*/
typedef struct _item_typeid_t {
	int uuid,type;
	char name[ITEM_NAMESZ];
} item_typeid_t;
/*----------------------------------------------------------------------------*/
const item_typeid_t ITEM_TYPEID[] = {
	{0x04,ITEM_TYPE_WEAPON,"Knife"},
	{0x07,ITEM_TYPE_WEAPON,"Spear"},
	{0x08,ITEM_TYPE_WEAPON,"10mm Pistol"},
	{0x09,ITEM_TYPE_WEAPON,"10mm SMG"},
	{0x10,ITEM_TYPE_WEAPON,"Laser Pistol"},
	{0x14,ITEM_TYPE_WEAPON,"Crowbar"},
	{0x15,ITEM_TYPE_WEAPON,"Brass Knuckles"},
	{0x16,ITEM_TYPE_WEAPON,"14mm Pistol"},
	{0x17,ITEM_TYPE_WEAPON,"Assault Rifle"},
	{0x18,ITEM_TYPE_WEAPON,"Plasma Pistol"},
	{0x2d,ITEM_TYPE_WEAPON,"Throwing Knife"},
	{0x4f,ITEM_TYPE_WEAPON,"Flare"},
	{0x78,ITEM_TYPE_WEAPON,"Alien Blaster"},
	{0x9f,ITEM_TYPE_WEAPON,"Molotov Cocktail"},
	{0xea,ITEM_TYPE_WEAPON,"Spiked Knuckles"},
	{0xec,ITEM_TYPE_WEAPON,"Combat Knife"},
	{0xf2,ITEM_TYPE_WEAPON,"Combat Shotgun"},
	{0x1d,ITEM_TYPE_AMMO,"10mm JHP"},
	{0x1e,ITEM_TYPE_AMMO,"10mm AP"},
	{0x20,ITEM_TYPE_AMMO,"14mm JHP"},
	{0x21,ITEM_TYPE_AMMO,"14mm AP"},
	{0x23,ITEM_TYPE_AMMO,"5mm JHP"},
	{0x24,ITEM_TYPE_AMMO,"5mm AP"},
	{0x26,ITEM_TYPE_AMMO,"Small Energy Cell"},
	{0x27,ITEM_TYPE_AMMO,"Micro Fusion Cell"},
	{0x5f,ITEM_TYPE_AMMO,"12 ga. Shotgun Shells"},
	{0x03,ITEM_TYPE_ARMOR,"Powered Armor"},
	{0x11,ITEM_TYPE_ARMOR,"Combat Armor"},
	{0xef,ITEM_TYPE_ARMOR,"Brotherhood Armor"},
	{0x28,ITEM_TYPE_DRUG,"Stimpak"},
	{0x30,ITEM_TYPE_DRUG,"RadAway"},
	{0x31,ITEM_TYPE_DRUG,"Antidote"},
	{0x57,ITEM_TYPE_DRUG,"Buffout"},
	{0x6d,ITEM_TYPE_DRUG,"Rad-X"},
	{0x90,ITEM_TYPE_DRUG,"Super Stimpak"},
	{0x47,ITEM_TYPE_CONSUMABLE,"Fruit"},
	{0x51,ITEM_TYPE_CONSUMABLE,"Iguana-on-a-Stick"},
	{0x67,ITEM_TYPE_CONSUMABLE,"Iguana-on-a-Stick"},
	{0x6a,ITEM_TYPE_CONSUMABLE,"Nuka-Cola"},
	{0x7d,ITEM_TYPE_CONSUMABLE,"Booze"},
	{0x29,ITEM_TYPE_MISC,"Bottle Caps"},
	{0x2f,ITEM_TYPE_MISC,"First Aid Kit"},
	{0x34,ITEM_TYPE_MISC,"Geiger Counter"},
	{0x3b,ITEM_TYPE_MISC,"Motion Sensor"},
	{0x4b,ITEM_TYPE_MISC,"Tool"},
	{0x49,ITEM_TYPE_MISC,"Big Book of Science"},
	{0x54,ITEM_TYPE_MISC,"Lock Picks"},
	{0x64,ITEM_TYPE_MISC,"Radio"},
	{0x65,ITEM_TYPE_MISC,"Lighter"},
	{0x7e,ITEM_TYPE_MISC,"Water Flask"},
	{0x7f,ITEM_TYPE_MISC,"Rope"},
	{0xc4,ITEM_TYPE_MISC,"Unknown Holodisk"},
	{0xd8,ITEM_TYPE_MISC,"Holodisk (BoS Maxson)"},
	{0xdf,ITEM_TYPE_MISC,"Yellow Pass Key"},
	{0xe6,ITEM_TYPE_MISC,"Vault Location Disk"},
	{0xee,ITEM_TYPE_MISC,"Holodisk (Regu.Trans.)"},
	{ITEM_TYPE_UNKNOWN,ITEM_TYPE_UNKNOWN,"Unknown"},
};
/*----------------------------------------------------------------------------*/
int item_valid8(item_t* item) {
	int stat = 1;
	if (item->checkFF0!=0xffffffff||item->checkFF1!=0xffffffff)
		stat = 0;
	else if (item->check000||item->check001) stat = 0;
	return stat;
}
/*----------------------------------------------------------------------------*/
item_typeid_t* item_typeid(unsigned int type) {
	item_typeid_t* pchk;
	pchk = (item_typeid_t*)ITEM_TYPEID;
	while (pchk->uuid!=ITEM_TYPE_UNKNOWN) {
		if (type==pchk->uuid) break;
		pchk++;
	}
	return pchk;
}
/*----------------------------------------------------------------------------*/
int item_type(unsigned int type) {
	item_typeid_t* pchk;
	pchk = item_typeid(type);
	return pchk->type;
}
/*----------------------------------------------------------------------------*/
int item_name(unsigned int type) {
	item_typeid_t* pchk;
	pchk = item_typeid(type);
	if (pchk->type==ITEM_TYPE_UNKNOWN)
		return pchk->type;
	printf("%s",pchk->name);
	return 0;
}
/*----------------------------------------------------------------------------*/
void toggle_item_end(item_t* pstat) {
	int loop;
	unsigned int *pdata = (unsigned int*) pstat;
	for (loop=0;loop<sizeof(item_t)/sizeof(int);loop++) {
		big2lit_end((void*)pdata,sizeof(int));
		pdata++;
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1FALLOUT_ITEM_H__ */
/*----------------------------------------------------------------------------*/
