/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "fallout_show.h"
#include "fallout_play.h"
/*----------------------------------------------------------------------------*/
char* basename(char* pname, int nodot) {
	int index = -1, index_d = -1, size = -1, count = 0, loop;
	int path_sep = '/';
	if (pname[1]==':') path_sep = '\\';
	while (pname[count]!='\0') count++;
	for (loop=count-1;loop>=0;loop--) {
		if (pname[loop]=='.') {
			if (index_d<0) index_d = loop;
		}
		if (pname[loop]==path_sep) {
			index = loop + 1;
			size = count - index;
			if (nodot&&index_d>=0)
				size -= count - index_d;
			break;
		}
	}
	if (index>=0) {
		for (loop=0;loop<size;loop++) {
			pname[loop] = pname[loop+index];
		}
		pname[size] = '\0';
	}
	return pname;
}
/*----------------------------------------------------------------------------*/
#define FLAG_FALLOUT1 0x00010000
#define FLAG_FALLOUT2 0x00020000
#define FLAG_HACK 0x00000001
#define FLAG_SAVE 0x00000010
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	FILE *file;
	player_t self;
	playstat_t stat;
	item_t item;
	unsigned int flag;
	unsigned int *pdata;
	int size, test, loop, save, debug = 0;
	unsigned int sizeof_GVAR = SIZEOF_GVAR_FALLOUT1;
	unsigned int offset_4hack = OFFSET_GVAR;
	putchar('\n');
	basename(argv[0],1);
	printf("%s - Fallout 1/2 SAVE.DAT File Editor\n\n", argv[0]);
	if (argc<2) {
		printf("Usage: %s <filename> [options]\n\n",argv[0]);
		printf("Options:\n");
		printf("       --max   : show max'd values\n");
		printf("       --save  : write max'd values back to file\n");
		printf("       --debug : debug mode\n");
		printf("       --fo2   : Fallout2 game file\n\n");
		return -2;
	}
	for (loop=2;loop<argc;loop++) {
		if (!strncmp(argv[loop],"--max",7)) flag |= FLAG_HACK;
		else if (!strncmp(argv[loop],"--save",7)) flag |= (FLAG_HACK|FLAG_SAVE);
		else if (!strncmp(argv[loop],"--debug",8)) debug = 1;
		else if (!strncmp(argv[loop],"--fo2",6)) flag |= FLAG_FALLOUT2;
		else fprintf(stderr,"** Unknown option '%s'!\n\n",argv[loop]);
	}
	if (flag&FLAG_FALLOUT2) {
		sizeof_GVAR = SIZEOF_GVAR_FALLOUT2; /* hacking for fallout2 instead! */
		printf("-- Hacking Fallout 2!\n\n");
	}
	offset_4hack += sizeof_GVAR;
	file = fopen(argv[1],"rb+");
	if (file) {
		do {
			/* remove path from filename */
			basename(argv[1],0);
			/* try to get file size */
			fseek(file,0L,SEEK_END);
			size = ftell(file);
			printf("FileName: %s\n",argv[1]);
			printf("Size: %d byte(s)\n\n",size);
			/* seek known offset - maps @FUN3 */
			fseek(file,offset_4hack,SEEK_SET);
			if (debug) {
				test = ftell(file);
				printf("FUN3 Offset: %d (%08X)\n",test,test);
			}
			fread((void*)&test,1,sizeof(test),file);
			big2lit_end((void*)&test,sizeof(test));
			if (debug) /* Map Count - Getting NULL-terminated names next */
				printf("MapCount: %d\n",test);
			for (loop=0;loop<test;loop++) {
				while(fgetc(file)!=0x00);
			}
			fread((void*)&test,1,sizeof(test),file);
			big2lit_end((void*)&test,sizeof(test));
			if (debug) /* Ignored Value? size of AUTOMAP.DB */
				printf("AutoMapSize: %d\n",test);
			/* seek known offset - player & inventory @FUN5 */
			fseek(file,sizeof_GVAR,SEEK_CUR);
			if (debug) {
				test = ftell(file);
				printf("FUN5 Offset: %d (%08X)\n",test,test);
			}
			/* get player info! */
			fread((void*)&self,1,sizeof(player_t),file);
			pdata = (unsigned int*) &self.datack;
			big2lit_end((void*)pdata,sizeof(unsigned int));
			if (*pdata!=KNOWN_DATACK) {
				printf("Check failed! (%08X)!=(%08X)\n",*pdata,KNOWN_DATACK);
				break;
			}
			big2lit_end((void*)&self.item_count,sizeof(unsigned int));
			if (debug) {
				test = ftell(file);
				printf("Item Count: %d\n",self.item_count);
				printf("Item Offset: %d (%08X)\n",test,test);
			}
			/* loop through items */
			for (loop=0;loop<self.item_count;loop++) {
				fread((void*)&item,sizeof(int),ITEM_BASE_COUNT,file);
				toggle_item_end(&item);
				if (debug) {
					printf("  Item#%02d: ",(loop+1));
					if (item_name(item.objectid))
						printf("%08X",item.objectid);
					printf("\n");
					if (item.location!=ITEM_INVENTORY)
						printf("    Location: [%08X]\n",item.location);
					printf("    Quantity: %d\n",item.quantity);
					printf("    Unknown0: %08X\n",item.unknown0);
					printf("    InUseChk: %08X",item.inuse_location);
					if (item.inuse_location&ITEM_USE_HAND1)
						printf(" [Slot1]");
					else if (item.inuse_location&ITEM_USE_HAND2)
						printf(" [Slot2]");
					else if (item.inuse_location&ITEM_USE_ARMOR)
						printf(" [Armor]");
					printf("\n");
				}
				if (!item_valid8(&item)) printf("** Invalid item?!\n");
				if (item_type(item.objectid)>=ITEM_TYPE_WEAPON) {
					fread((void*)&item.ammo_count,
						sizeof(unsigned int),1,file);
					if (debug) {
						big2lit_end((void*)&item.ammo_count,
							sizeof(unsigned int));
						printf("    Count000: %d\n",(int)item.ammo_count);
					}
				}
				if (item_type(item.objectid)==ITEM_TYPE_WEAPON) {
					fread((void*)&item.ammo_type,
						sizeof(unsigned int),1,file);
					if (debug) {
						big2lit_end((void*)&item.ammo_type,
							sizeof(unsigned int));
						printf("    AmmoType: ");
						if (item.ammo_type==0xffffffff) printf("N/A");
						else if (item_name(item.ammo_type))
							printf("%08X",item.ammo_type);
						printf("\n");
					}
				}
			}
			/* should be at player stat @FUN6 */
			save = ftell(file);
			if (debug)
				printf("FUN6 Offset: %d (%08X)\n",save,save);
			/* get playstat */
			fread((void*)&stat,sizeof(playstat_t),1,file);
			/* correct endian */
			toggle_playstat_end(&stat);
			if (debug) {
				printf("PlayStat00: %08X\n",stat.chk0);
				printf("PlayStatVx: %08X\n",stat.valX);
				printf("\n");
			}
			/* show me! */
			print_stat(&stat.stat);
			/* simple mod for now! */
			if (flag&FLAG_HACK) {
				test = stat_hackX(&stat.stat);
				if (test) {
					printf("\nMODDED!\n\n");
					print_stat(&stat.stat);
				}
			}
			else test = 0;
			/* restore endian */
			toggle_playstat_end(&stat);
			/* reset file pointer position - for writeback! */
			if (test&&(flag&FLAG_SAVE)) {
				fseek(file,save,SEEK_SET);
				test = fwrite((void*)&stat,1,sizeof(playstat_t),file);
				printf("[HACKED!] => %d byte(s) (%d)\n\n",
					test,(int)sizeof(playstat_t));
			}
		} while(0);
		/* that's it! */
		fclose(file);
	}
	else fprintf(stderr,"** Error opening file '%s'!\n\n",argv[1]);
	if (sizeof(playstat_t)!=PLAYSTAT_SIZE) {
		fprintf(stderr,"** Player Stat Size error! (%d|%d)\n\n",
			(int)sizeof(playstat_t),PLAYSTAT_SIZE);
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
