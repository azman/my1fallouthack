/*----------------------------------------------------------------------------*/
#ifndef __MY1FALLOUT_GCD_H__
#define __MY1FALLOUT_GCD_H__
/*----------------------------------------------------------------------------*/
#include "fallout_stat.h"
/*----------------------------------------------------------------------------*/
typedef struct _base_info_t {
	char name[32];
	/* offset: 32 */
	unsigned int tagged_skill[4]; /* fourth is 0xFFFFFFFF (perks) */
	/* offset: 32+(4x4)=48 */
	unsigned int traits[2];
	/* offset: 48+(4x2)=56 >> character points for stats (0-70) */
	unsigned int end0;
	/* size: 60 */
} base_info_t;
/*----------------------------------------------------------------------------*/
void toggle_info_end(base_info_t* pinfo) {
	int loop;
	unsigned int *pdata = pinfo->tagged_skill;
	for (loop=0;loop<7;loop++) {
		big2lit_end((void*)pdata,sizeof(int));
		pdata++;
	}
}
/*----------------------------------------------------------------------------*/
typedef struct _gcd_t {
	base_stat_t stat;
	unsigned int xtra; /* only in fallout2? */
	/* offset: 368 (fallout1) , 372 (fallout2) */
	base_info_t info;
	/* size: 428 (fallout1) , 432 (fallout2) */
} gcd_t;
/*----------------------------------------------------------------------------*/
void toggle_gcd_end(gcd_t* pgcd) {
	toggle_stat_end(&pgcd->stat);
	big2lit_end((void*)&pgcd->xtra,sizeof(int));
	toggle_info_end(&pgcd->info);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1FALLOUT_GCD_H__ */
/*----------------------------------------------------------------------------*/
