/*----------------------------------------------------------------------------*/
#ifndef __MY1FALLOUT_PLAY_H__
#define __MY1FALLOUT_PLAY_H__
/*
https://falloutmods.fandom.com/wiki/SAVE.DAT_File_Format
https://falloutmods.fandom.com/wiki/SAVE.DAT_File_Format#Inventory_item_format
*/
/*----------------------------------------------------------------------------*/
#include "fallout_head.h"
#include "fallout_stat.h"
#include "fallout_item.h"
/*----------------------------------------------------------------------------*/
#define OFFSET_FUN0 SIZEOF_HEAD
#define SIZEOF_FUN0 0x0000
#define OFFSET_FUN1 (OFFSET_FUN0+SIZEOF_FUN0)
#define SIZEOF_FUN1 0x0004
#define OFFSET_GVAR (OFFSET_FUN1+SIZEOF_FUN1)
#define SIZEOF_GVAR_FALLOUT1 0x09A9
#define SIZEOF_GVAR_FALLOUT2 0x0AE0
/* 0x00,0x00,'F','P' >> fist 4 bytes in player_t */
#define KNOWN_DATACK 0x00004650
/*----------------------------------------------------------------------------*/
typedef struct _player_t {
	unsigned int datack; /* KNOWN_DATACK */
	unsigned int mappos;
	unsigned int unknown01[5];
	unsigned int mapbrg; /* 0-NE 1-E 2-SE 3-SW 4-W 5-NW */
	unsigned int fidnum; /* apperance (e.g. 0x0100000b for jumpsuit) */
	unsigned int flag00; /* usu. 0x60002420 last byte's 0x10 sneak mode! */
	unsigned int maplvl;
	unsigned int unknown02[7];
	/* offset:0x48 @72 @4x18 */
	unsigned int item_count;
	unsigned int unknown03[6];
	unsigned int state0; /* cripple flag: 0x0001=eyes, 0x0080=dead */
	unsigned int unknown04[3];
	/* offset:0x74 0x48+0x28 @116=72+4x11 */
	unsigned int current_hp;
	unsigned int radiation_lvl;
	unsigned int poison_lvl;
	/* size:0x80 @128 bytes */
} player_t;
/*----------------------------------------------------------------------------*/
typedef struct _playstat_t {
	/* doc'ed as at offset:0x0174 @372 bytes */
	/* 0x1450+valX is desc. line for critter type */
	unsigned int valX;
	unsigned int chk0; /* unknown */
	base_stat_t stat;
	/* size:0x0178 @376 bytes */
} playstat_t;
/*----------------------------------------------------------------------------*/
#define PLAYSTAT_SIZE 0x0178
/*----------------------------------------------------------------------------*/
void toggle_playstat_end(playstat_t* pstat) {
	int loop;
	unsigned int *pdata = (unsigned int*) pstat;
	for (loop=0;loop<sizeof(playstat_t)/sizeof(int);loop++) {
		big2lit_end((void*)pdata,sizeof(int));
		pdata++;
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1FALLOUT_PLAY_H__ */
/*----------------------------------------------------------------------------*/
