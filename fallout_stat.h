/*----------------------------------------------------------------------------*/
#ifndef __MY1FALLOUT_STAT_H__
#define __MY1FALLOUT_STAT_H__
/*----------------------------------------------------------------------------*/
/**
 *  primary stats (values: 1-10)
 *  - strength, perception, endurance, charisma
 *  - intelligence, agility, luck
**/
#define STAT_ST 0
#define STAT_PE 1
#define STAT_EN 2
#define STAT_CH 3
#define STAT_IN 4
#define STAT_AG 5
#define STAT_LK 6
#define STAT_PRIMARY_COUNT 7
/*----------------------------------------------------------------------------*/
const char STAT_PRI_NAMES[STAT_PRIMARY_COUNT][16] = {
	{"Strength"},{"Perception"},{"Endurance"},{"Charisma"},
	{"Intelligence"},{"Agility"},{"Luck"}
};
/*----------------------------------------------------------------------------*/
/**
 *  secondary stats
 *  - hit_points, action_points, armor_class (values: 0-999)
 *  - unarmed_damage (NOT USED), melee_damage, carry_weight (values: 0-999)
 *  - sequence, healing_rate (values: 0-99)
 *  - critical_chance, critical_hit_modifier (values: 0-100)
 *  - dmg_thresh : normal, laser, fire, plasma,
 *      electrical, emp, explosive (values: 0-999)
 *  - dmg_res : normal, laser, fire, plasma,
 *      electrical, emp, explosive (values: 0-100)
 *  - radiation resistance, poison resistance (values: 0-100)
 *  - age, gender (values: age to 35, gender 0 is male)
**/
#define STAT_HP 0
#define STAT_AP 1
#define STAT_AC 2
#define STAT_UD 3
#define STAT_MD 4
#define STAT_CW 5
#define STAT_SQ 6
#define STAT_HR 7
#define STAT_CC 8
#define STAT_CM 9
#define STAT_DT0 10
#define STAT_DT1 11
#define STAT_DT2 12
#define STAT_DT3 13
#define STAT_DT4 14
#define STAT_DT5 15
#define STAT_DT6 16
#define STAT_DR0 17
#define STAT_DR1 18
#define STAT_DR2 19
#define STAT_DR3 20
#define STAT_DR4 21
#define STAT_DR5 22
#define STAT_DR6 23
#define STAT_RR 24
#define STAT_PR 25
#define STAT_AGE 26
#define STAT_GENDER 27
#define STAT_SECOND_COUNT 28
/*----------------------------------------------------------------------------*/
const char STAT_SEC_NAMES[STAT_SECOND_COUNT][24] = {
	{"Hit Points"},{"Action Points"},{"Armor Class"},
	{"Unarmed(N/A)"},{"Melee Damage"},{"Carry Weight"},
	{"Sequence"},{"Healing Rate"},{"Critical Chance"},
	{"Critical Hit Modifier"},{"DT Normal"},{"DT Laser"},
	{"DT Fire"},{"DT Plasma"},{"DT Electrical"},{"DT EMP"},
	{"DT Explosives"},{"DR Normal"},{"DR Laser"},
	{"DR Fire"},{"DR Plasma"},{"DR Electrical"},{"DR EMP"},
	{"DR Explosives"},{"Radiation Res."},{"Poison Res."},
	{"Age"},{"Gender"}
};
/*----------------------------------------------------------------------------*/
/* skills */
#define SKILL_SMALL_GUNS 0
#define SKILL_BIG_GUNS 1
#define SKILL_ENERGY_WEAPONS 2
#define SKILL_UNARMED 3
#define SKILL_MELEE_WEAPONS 4
#define SKILL_THROWING 5
#define SKILL_FIRST_AID 6
#define SKILL_DOCTOR 7
#define SKILL_SNEAK 8
#define SKILL_LOCKPICK 9
#define SKILL_STEAL 10
#define SKILL_TRAPS 11
#define SKILL_SCIENCE 12
#define SKILL_REPAIR 13
#define SKILL_SPEECH 14
#define SKILL_BARTER 15
#define SKILL_GAMBLING 16
#define SKILL_OUTDOORSMAN 17
#define SKILL_COUNT 18
/*----------------------------------------------------------------------------*/
const char SKILL_NAMES[SKILL_COUNT][24] = {
	{"Small Guns"},{"Big Guns"},{"Energy Weapons"},{"Unarmed"},
	{"Melee Weapons"},{"Throwing"},{"First Aid"},{"Doctor"},{"Sneak"},
	{"Lockpick"},{"Steal"},{"Traps"},{"Science"},{"Repair"},{"Speech"},
	{"Barter"},{"Gambling"},{"Outdoorsman"}
};
/*----------------------------------------------------------------------------*/
/* traits */
#define TRAIT_FAST_METABOLISM 0
#define TRAIT_BRUISER 1
#define TRAIT_SMALL_FRAME 2
#define TRAIT_ONE_HANDER 3
#define TRAIT_FINESSE 4
#define TRAIT_KAMIKAZE 5
#define TRAIT_HEAVY_HANDED 6
#define TRAIT_FAST_SHOT 7
#define TRAIT_BLOODY_MESS 8
#define TRAIT_JINXED 9
#define TRAIT_GOOD_NATURED 10
#define TRAIT_CHEM_RELIANT 11
#define TRAIT_CHEM_RESISTANT 12
#define TRAIT_SEX_APPEAL 13
#define TRAIT_SKILLED 14
#define TRAIT_GIFTED 15
#define TRAIT_COUNT 16
/*----------------------------------------------------------------------------*/
const char TRAIT_NAMES[TRAIT_COUNT][24] = {
	{"Fast Metabolism"},{"Bruiser"},{"Small Frame"},{"One Hander"},
	{"Finesse"},{"Kamikaze"},{"Heavy Handed"},{"Fast Shot"},
	{"Bloody Mess"},{"Jinxed"},{"Good Natured"},{"Chem Reliant"},
	{"Chem Resistant"},{"Sex Appeal"},{"Skilled"},{"Gifted"}
};
/*----------------------------------------------------------------------------*/
#define STAT_FLAG_SNEAK  0x0001
#define STAT_FLAG_LEVEL  0x0008
#define STAT_FLAG_ADDICT 0x0010
/*----------------------------------------------------------------------------*/
typedef struct _base_stat_t {
	unsigned int flag;
	/* primary stat 0 - 10 */
	unsigned int primary[STAT_PRIMARY_COUNT];
	/* secondary stat */
	unsigned int secondary[STAT_SECOND_COUNT];
	/* bonus for primary */
	unsigned int bonus_primary[STAT_PRIMARY_COUNT];
	/* bonus for secondary */
	unsigned int bonus_secondary[STAT_SECOND_COUNT];
	/* skills => offset: (1+7+28+7+28)x4=284 */
	unsigned int skills[SKILL_COUNT];
	/* offset: 284+(18x4)=356 */
	unsigned int unknown[3];
	/* size: 368 */
} base_stat_t;
/*----------------------------------------------------------------------------*/
#define STAT_PRI_MAXVAL 10
#define STAT_HACK_SKILLS 20
#define STAT_HACK_ACTION_POINT 10
#define STAT_HACK_CARRY_WEIGHT 250
#define STAT_HACK_MELEE_DAMAGE 100
#define STAT_HACK_HEALING_RATE 5
#define STAT_HACK_CRITICAL_PCT 60
#define STAT_HACK_CRITICAL_MOD 50
#define STAT_HACK_DAMAGE_RESIST 60
#define STAT_HACK_DAMAGE_THRESH 50
/*----------------------------------------------------------------------------*/
#define STAT_HACK_MESG "[HACK]\n\n"
/*----------------------------------------------------------------------------*/
int stat_hack_primes(base_stat_t* pstat, int hackd) {
	int loop;
	/* max all primary stats */
	for (loop=0;loop<STAT_PRIMARY_COUNT;loop++) {
		if (pstat->primary[loop]!=STAT_PRI_MAXVAL) {
			if (!hackd) printf(STAT_HACK_MESG);
			printf("-- %s: %d >> ",
				STAT_PRI_NAMES[loop],pstat->primary[loop]);
			pstat->primary[loop] = STAT_PRI_MAXVAL;
			printf("%d (MAX)\n",pstat->primary[loop]);
			hackd++;
		}
	}
	return hackd;
}
/*----------------------------------------------------------------------------*/
int stat_hack_skills(base_stat_t* pstat, int hackd, int skill, int super) {
	if (pstat->skills[skill]<super) {
		if (!hackd) printf(STAT_HACK_MESG);
		printf("-- %s: %d >> ",SKILL_NAMES[skill],pstat->skills[skill]);
		pstat->skills[skill] = super;
		printf("%d\n",pstat->skills[skill]);
		hackd++;
	}
	return hackd;
}
/*----------------------------------------------------------------------------*/
int stat_hack_seconds(base_stat_t* pstat, int hackd, int spick, int super) {
	if (pstat->bonus_secondary[spick]<super) {
		if (!hackd) printf(STAT_HACK_MESG);
		printf("-- %s: %d >> ",
			STAT_SEC_NAMES[spick],pstat->bonus_secondary[spick]);
		pstat->bonus_secondary[spick] = super;
		printf("%d\n",pstat->bonus_secondary[spick]);
		hackd++;
	}
	return hackd;
}
/*----------------------------------------------------------------------------*/
int stat_hack_skills_up(base_stat_t* pstat, int hackd) {
	int loop;
	/* round up all skills */
	for (loop=0;loop<SKILL_COUNT;loop++) {
		if (pstat->skills[loop]<STAT_HACK_SKILLS) {
			if (!hackd) printf(STAT_HACK_MESG);
			printf("-- %s: %d >> ",SKILL_NAMES[loop],pstat->skills[loop]);
			pstat->skills[loop] = ((pstat->skills[loop]/10)+1)*10;
			printf("%d\n",pstat->skills[loop]);
			hackd++;
		}
	}
	return hackd;
}
/*----------------------------------------------------------------------------*/
int stat_hack_seconds_damage(base_stat_t* pstat, int hackd) {
	int loop, chkR, chkT;
	chkR = STAT_DR0; chkT = STAT_DT0;
	/* all types */
	for (loop=0;loop<7;loop++,chkR++,chkT++) {
		if (pstat->secondary[chkR]<STAT_HACK_DAMAGE_RESIST) {
			if (!hackd) printf(STAT_HACK_MESG);
			printf("-- %s: %d >> ",STAT_SEC_NAMES[chkR],pstat->secondary[chkR]);
			pstat->secondary[chkR] = STAT_HACK_DAMAGE_RESIST;
			printf("%d\n",pstat->secondary[chkR]);
			hackd++;
		}
		if (pstat->secondary[chkT]<STAT_HACK_DAMAGE_THRESH) {
			if (!hackd) printf(STAT_HACK_MESG);
			printf("-- %s: %d >> ",STAT_SEC_NAMES[chkT],pstat->secondary[chkT]);
			pstat->secondary[chkT] = STAT_HACK_DAMAGE_THRESH;
			printf("%d\n",pstat->secondary[chkT]);
			hackd++;
		}
	}
	return hackd;
}
/*----------------------------------------------------------------------------*/
int stat_hack0(base_stat_t* pstat) {
	int test = 0; /* mod indicator */
	test = stat_hack_primes(pstat,test);
	/* selected skills */
	test = stat_hack_skills(pstat,test,SKILL_SMALL_GUNS,STAT_HACK_SKILLS);
	test = stat_hack_skills(pstat,test,SKILL_BIG_GUNS,STAT_HACK_SKILLS);
	test = stat_hack_skills(pstat,test,SKILL_ENERGY_WEAPONS,STAT_HACK_SKILLS);
	test = stat_hack_skills(pstat,test,SKILL_MELEE_WEAPONS,STAT_HACK_SKILLS);
	test = stat_hack_skills(pstat,test,SKILL_LOCKPICK,STAT_HACK_SKILLS);
	test = stat_hack_skills(pstat,test,SKILL_REPAIR,STAT_HACK_SKILLS);
	test = stat_hack_skills(pstat,test,SKILL_SPEECH,STAT_HACK_SKILLS);
	test = stat_hack_skills(pstat,test,SKILL_OUTDOORSMAN,STAT_HACK_SKILLS);
	/* damage resistance/thresholds */
	test = stat_hack_seconds_damage(pstat,test);
	return test;
}
/*----------------------------------------------------------------------------*/
int stat_hackX(base_stat_t* pstat) {
	int test = 0; /* mod indicator */
	test = stat_hack_primes(pstat,test);
	test = stat_hack_skills_up(pstat,test);
	/**
	 * Secondary stats:
	 * - only in save.dat?
	 * - in gcd, only damage thresh./resist. & age/gender gets into game
	 *   = maybe try set to bonus when in gcd?
	**/
	/* do more! */
	test = stat_hack_seconds(pstat,test,STAT_AP,STAT_HACK_ACTION_POINT);
	/* carry more things! */
	test = stat_hack_seconds(pstat,test,STAT_CW,STAT_HACK_CARRY_WEIGHT);
	/* faster healing! */
	test = stat_hack_seconds(pstat,test,STAT_HR,STAT_HACK_HEALING_RATE);
	/* killer melee damage! */
	test = stat_hack_seconds(pstat,test,STAT_MD,STAT_HACK_MELEE_DAMAGE);
	/* criticals! */
	test = stat_hack_seconds(pstat,test,STAT_CC,STAT_HACK_CRITICAL_PCT);
	test = stat_hack_seconds(pstat,test,STAT_CM,STAT_HACK_CRITICAL_MOD);
	return test;
}
/*----------------------------------------------------------------------------*/
void big2lit_end(void *src, int size) {
	unsigned char *chk = (unsigned char*) src, *nxt = &chk[size-1];
	unsigned char swap;
	int loop;
	for (loop=0;loop<size/2;loop++) {
		swap = *chk;
		*chk = *nxt;
		*nxt = swap;
		chk++; nxt--;
	}
}
/*----------------------------------------------------------------------------*/
void toggle_stat_end(base_stat_t* pstat) {
	int loop;
	unsigned int *pdata = (unsigned int*) pstat;
	for (loop=0;loop<sizeof(base_stat_t)/sizeof(int);loop++) {
		big2lit_end((void*)pdata,sizeof(int));
		pdata++;
	}
}
/*----------------------------------------------------------------------------*/
typedef struct _skill_comp_t {
	int base,modw,stat1,stat2;
} skill_comp_t;
/*----------------------------------------------------------------------------*/
const skill_comp_t SKILL_CALC[SKILL_COUNT] = {
	/*  fallout1 20250208 */
	{35,1,STAT_AG,-1},
	{10,1,STAT_AG,-1},
	{10,1,STAT_AG,-1},
	{65,-2,STAT_AG,STAT_ST},
	{55,-2,STAT_AG,STAT_ST},
	{40,1,STAT_AG,-1},
	{30,-2,STAT_PE,STAT_IN}, /* in-game info shows 30,1 */
	{15,-2,STAT_PE,STAT_IN},
	{25,1,STAT_AG,-1},
	{20,-2,STAT_PE,STAT_AG},
	{20,1,STAT_AG,-1},
	{20,-2,STAT_PE,STAT_AG},
	{25,2,STAT_IN,-1},
	{20,1,STAT_IN,-1},
	{25,2,STAT_CH,-1},
	{20,2,STAT_CH,-1},
	{20,3,STAT_LK,-1},
	{5,-2,STAT_EN,STAT_IN}
};
/*----------------------------------------------------------------------------*/
int check_skill(base_stat_t *pstat, int skill_index) {
	int test, chk1, chk2, temp;
	chk1 = SKILL_CALC[skill_index].stat1;
	chk2 = SKILL_CALC[skill_index].stat2;
	temp = SKILL_CALC[skill_index].modw;
	/* first stat */
	test = pstat->primary[chk1];
	/* opt. second stat */
	if (chk2>=0)
		test += pstat->primary[chk2];
	/* weight */
	if (temp<0) {
		temp = -temp;
		test /= temp;
	}
	else if (temp>0) test *= temp;
	/* offset */
	test += SKILL_CALC[skill_index].base;
	return test;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1FALLOUT_STAT_H__ */
/*----------------------------------------------------------------------------*/
