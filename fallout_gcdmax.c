/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "fallout_show.h"
/*----------------------------------------------------------------------------*/
#define NAME_OFFSET1 368
#define NAME_OFFSET2 372
#define SIZE_THEREST 60
/*----------------------------------------------------------------------------*/
char* basename(char* pname, int nodot) {
	int index = -1, index_d = -1, size = -1, count = 0, loop;
	int path_sep = '/';
	if (pname[1]==':') path_sep = '\\';
	while (pname[count]!='\0') count++;
	for (loop=count-1;loop>=0;loop--) {
		if (pname[loop]=='.') {
			if (index_d<0) index_d = loop;
		}
		if (pname[loop]==path_sep) {
			index = loop + 1;
			size = count - index;
			if (nodot&&index_d>=0)
				size -= count - index_d;
			break;
		}
	}
	if (index>=0) {
		for (loop=0;loop<size;loop++) {
			pname[loop] = pname[loop+index];
		}
		pname[size] = '\0';
	}
	return pname;
}
/*----------------------------------------------------------------------------*/
#define FLAG_FALLOUT1 0x00010000
#define FLAG_HACK 0x00000001
#define FLAG_SAVE 0x00000010
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	FILE *file;
	gcd_t gcdd;
	unsigned int flag;
	int size, test, loop;
	flag = 0;
	putchar('\n');
	basename(argv[0],1);
	printf("%s - Fallout 1/2 GCD To MAX\n\n", argv[0]);
	if (argc<2) {
		printf("Usage: %s <filename> [options]\n\n",argv[0]);
		printf("Options:\n");
		printf("       --max : show max'd values\n");
		printf("       --save : write max'd values back to file\n\n");
		return -2;
	}
	for (loop=2;loop<argc;loop++) {
		if (!strncmp(argv[loop],"--max",7)) flag |= FLAG_HACK;
		else if (!strncmp(argv[loop],"--save",7)) flag |= (FLAG_HACK|FLAG_SAVE);
		else fprintf(stderr,"** Unknown option '%s'!\n",argv[loop]);
	}
	file = fopen(argv[1],"rb+");
	if (file) {
		do {
			/* remove path from filename */
			basename(argv[1],0);
			/* try to get file size */
			fseek(file,0L,SEEK_END);
			size = ftell(file);
			printf("FileName: %s\n",argv[1]);
			printf("Size: %d byte(s)\n\n",size);
			if ((size+sizeof(int))==sizeof(gcd_t))
				flag |= FLAG_FALLOUT1; /* fallout 1? */
			else if (size!=sizeof(gcd_t)) {
				printf("[ERROR] Expecting %d bytes! "
					"(or %d bytes less for FO1)\n\n",
					(int)sizeof(gcd_t),(int)sizeof(int));
				break;
			}
			/* reset file for read */
			fseek(file,0L,SEEK_SET);
			if (flag&FLAG_FALLOUT1) {
				fread((void*)&gcdd.stat,sizeof(base_stat_t),1,file);
				gcdd.xtra = 0x00000000; /* just in case */
				fread((void*)&gcdd.info,sizeof(base_info_t),1,file);
			}
			else fread((void*)&gcdd,sizeof(gcd_t),1,file);
			/* correct endian */
			toggle_gcd_end(&gcdd);
			/* show me! */
			print_gcd_data(&gcdd);
			if (flag&FLAG_HACK) {
				test = stat_hack0(&gcdd.stat);
				if (test) {
					printf("\nMODDED!\n\n");
					print_gcd_data(&gcdd);
				}
			}
			else test = 0; /* mod indicator */
			/* restore endian */
			toggle_gcd_end(&gcdd);
			/* reset file pointer position - for writeback! */
			if (test&&(flag&FLAG_SAVE)) {
				fseek(file,0L,SEEK_SET);
				if (flag&FLAG_FALLOUT1) {
					test = fwrite((void*)&gcdd.stat,1,
						sizeof(base_stat_t),file);
					test += fwrite((void*)&gcdd.info,1,
						sizeof(base_info_t),file);
				}
				else test = fwrite((void*)&gcdd,1,sizeof(gcd_t),file);
				printf("[HACKED!] => %d byte(s)\n\n",test);
			}
		} while(0);
		/* that's it! */
		fclose(file);
	}
	else fprintf(stderr,"** Error opening file '%s'!\n\n",argv[1]);
	return 0;
}
/*----------------------------------------------------------------------------*/
