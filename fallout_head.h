/*----------------------------------------------------------------------------*/
#ifndef __MY1FALLOUT_HEAD_H__
#define __MY1FALLOUT_HEAD_H__
/*----------------------------------------------------------------------------*/
/*
 * Note:
 * - ASCII strings are 0x0-terminated 32-byte character array (31+1)
 * - All integers are big-endian
 * - SAVE.DAT access in chunks
 */
/*----------------------------------------------------------------------------*/
typedef struct _save_head_t {
	unsigned char sign[24]; /* 0x00:"FALLOUT SAVE FILE" */
	unsigned short int vmaj; /* 0x18:MajVers */
	unsigned short int vmin; /* 0x1A:MinVers */
	char chk0; /* 0x1C:'R' */
	char name[32]; /*  0x1d:Player Name */
	char save[30]; /*  0x3d:SavedGame Name */
	unsigned short int dday; /* 0x5b:day */
	unsigned short int mmin; /* 0x5d:month */
	unsigned short int year; /* 0x5f:year */
	unsigned int hhmm; /* 0x61:hour/mmin in 24-h format? */
	unsigned short int gmon; /* 0x65:in-game month */
	unsigned short int gday; /* 0x67:in-game day */
	unsigned short int gyer; /* 0x69:in-game year */
	unsigned int gmhm; /* 0x6b:game hour/mmin in 24-h format? */
	unsigned int nmap; /* 0x6f: high=maplevel, low=mapnumber */
	char mapl[16]; /*  0x73:Map name */
	unsigned char bmap[29792]; /* 0x83:bitmap image (size=0x7460) */
	unsigned char zpad[128]; /* 0x74e3: zero-pads (size=0x80) */
	/* size:0x7563 (0x74e3+0x80) = 30051 bytes*/
} save_head_t;
/*----------------------------------------------------------------------------*/
#define SIZEOF_HEAD 0x7563
/*----------------------------------------------------------------------------*/
#endif /** __MY1FALLOUT_HEAD_H__ */
/*----------------------------------------------------------------------------*/
