/*----------------------------------------------------------------------------*/
#ifndef __MY1FALLOUT_SHOW_H__
#define __MY1FALLOUT_SHOW_H__
/*----------------------------------------------------------------------------*/
#include "fallout_gcd.h"
/*----------------------------------------------------------------------------*/
void print_stat(base_stat_t* pstat) {
	int loop=0,test;
	printf("PRIMARY STATS");
	while (loop<STAT_PRIMARY_COUNT) {
		if (loop%2==0&&loop<STAT_PRIMARY_COUNT-1) printf("\n  ");
		else printf(" , ");
		printf("%s: %d (%d)",STAT_PRI_NAMES[loop],
			pstat->primary[loop],pstat->bonus_primary[loop]); loop++;
	}
	printf("\n\n");
	loop=0;
	printf("SECONDARY STATS");
	while (loop<STAT_SECOND_COUNT) {
		if (loop>=STAT_DT0&&loop<=STAT_DR6) {
			loop++; continue;
		}
		if(loop%2==0) printf("\n  ");
		else printf(" , ");
		printf("%s: %d (%d)",STAT_SEC_NAMES[loop],
			pstat->secondary[loop],pstat->bonus_secondary[loop]); loop++;
	}
	printf("\n\n");
	loop=0;
	printf("SKILLS");
	while (loop<SKILL_COUNT) {
		if (loop%3==0) printf("\n  ");
		else printf(" , ");
		test = pstat->skills[loop]+check_skill(pstat,loop);
		printf("%s: %d",SKILL_NAMES[loop],test);
		loop++;
	}
	printf("\n\n");
}
/*----------------------------------------------------------------------------*/
void print_info(base_info_t* pinfo) {
	printf("INFO\n");
	printf("  Name: %s\n",pinfo->name);
	printf("  Trait 1: %s\n",TRAIT_NAMES[pinfo->traits[0]]);
	printf("  Trait 2: %s\n",TRAIT_NAMES[pinfo->traits[1]]);
	printf("  Tag Skill 1: %s\n",SKILL_NAMES[pinfo->tagged_skill[0]]);
	printf("  Tag Skill 2: %s\n",SKILL_NAMES[pinfo->tagged_skill[1]]);
	printf("  Tag Skill 3: %s\n",SKILL_NAMES[pinfo->tagged_skill[2]]);
	printf("\n");
}
/*----------------------------------------------------------------------------*/
void print_gcd_data(gcd_t* pgcd) {
	print_stat(&pgcd->stat);
	print_info(&pgcd->info);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1FALLOUT_SHOW_H__ */
/*----------------------------------------------------------------------------*/
