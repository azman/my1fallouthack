# makefile for my1fallouthack

SOURCES = $(sort $(wildcard fallout*.c))
TARGETS = $(subst .c,,$(SOURCES))
HEADERS = $(sort $(wildcard fallout*.h))

CC = gcc
DELETE = rm -rf

CFLAGS += -Wall
LFLAGS = -static
ifneq ($(LDFLAGS),)
LFLAGS += $(LDFLAGS)
endif
OFLAGS =

.PHONY: all gcd clean new

all: $(TARGETS)

hack: fallout_hack

gcd: fallout_gcdmax

$(TARGETS): % : %.o
	$(CC) -o $@ $< $(LFLAGS) $(OFLAGS)
	strip $@

%.o: %.c  %.h $(HEADERS)
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: %.c $(HEADERS)
	$(CC) -c $(CFLAGS) -o $@ $<

clean:
	-$(DELETE) $(TARGETS) *.o

new: clean all
